open util/ordering[Time] as to
open util/natural as na

enum UTypes{Premium, Basic}
enum Mode{Regular, Secure, ReadOnly} //29. Every active file is shared in one of the possible 3 sharing modes;

one sig GitBob {
	register_user: User -> Time,
	files: Files -> Time, //12. GitBob only keeps track of the size, owner, and version of current files;
}

sig Time{}

sig User {
	email: one UEmail,
	type:  UTypes one -> Time,
	local_files: set Files -> Time,

}

sig UEmail {}

sig Files{
	size: one Natural, //11. For the sake of simplicity one assumes that every version of a file occupies the same space;
	version: Natural one -> Time, //37. Only active files are versioned in GitBob;
	owner: one User,
	shared: set User -> Time, //20. A GitBob file may be shared among several users;
	mode: Mode one -> Time
}

pred init[t: Time]{
	no GitBob.register_user.t // 4. At the beginning there are no users registered in GitBob;
	no GitBob.files.t // 13. At the beginning there are no files in GitBob; 23. At the beginning there are no shared files in GitBob;
}

pred newUser [t, t' : Time ,user: User, ty: UTypes, mail: UEmail] {
	//pre conditions
	user not in GitBob.register_user.t //5. Any user may register himself in GitBob as long as it is not registered yet;
	all users: GitBob.register_user.t | users.email != mail //3. The e-mails registered in GitBob are unique;
	//Actions
	user.type.t' = ty
	user.email = mail
	user.local_files.t' = none
	GitBob.register_user.t' = GitBob.register_user.t + user //1. Every user in USER can register in GitBob;
	//post conditions
	noGitBobFilesChangeExcept[t, t']
	noFileVersionChangeExcept[t,t', GitBob.files.t]
	noUserLocalFilesChangeExcept[t, t', GitBob.register_user.t]
	noFileSharesChangeExcept[t, t', GitBob.files.t]
}

pred removeUser [t, t' : Time ,user: User] {
	//pre conditions
	user in GitBob.register_user.t //6. Only registered users may be removed from GitBob;
	user not in GitBob.files.t.owner //14. One cannot remove from GitBob a user that is the owner of some file;
	user not in GitBob.files.t.shared.t //24. One cannot remove from GitBob users that are involved in some sharing of files;
	//Actions
	GitBob.register_user.t' = GitBob.register_user.t - user
	//post conditions
	noGitBobFilesChangeExcept[t, t']
	noFileVersionChangeExcept[t,t', GitBob.files.t]
	noUserLocalFilesChangeExcept[t, t', GitBob.register_user.t]
	noFileSharesChangeExcept[t, t', GitBob.files.t]
}

pred upgradePremium [t, t' : Time ,user: User] {
		//pre conditions
		user in GitBob.register_user.t	//7. Only registered users may be upgraded to profile type PREMIUM;
		user.type.t = Basic // 9. Only BASIC users may be upgraded to PREMIUM; 
		//Actions
		user.type.t' = Premium
		//post conditions
		noGitBobUsersEmailsChangeExcept[t, t']
		noGitBobFilesChangeExcept[t, t']
		noFileVersionChangeExcept[t,t', GitBob.files.t]
		noUserLocalFilesChangeExcept[t, t', GitBob.register_user.t]
		noFileSharesChangeExcept[t, t', GitBob.files.t]
}

pred downgradeBasic [t, t' : Time ,user: User] {
		//pre conditions
 		user in GitBob.register_user.t	//8. Only registered users may be downgraded to profile type BASIC;
		all f: GitBob.files.t  | f.mode.t = Secure => user not in f.shared.t // 31. A user in a SECURE sharing cannot downgrade his profile to BASIC; 
		user.type.t = Premium // 9. Only PREMIUM users may be upgraded to BASIC; 
		//Actions
		user.type.t' = Basic
		//post conditions
		noGitBobUsersEmailsChangeExcept[t, t']
		noGitBobFilesChangeExcept[t, t']
		noFileVersionChangeExcept[t,t', GitBob.files.t]
		noUserLocalFilesChangeExcept[t, t', GitBob.register_user.t]
		noFileSharesChangeExcept[t, t', GitBob.files.t]
}

pred addFile  [t, t' : Time , file: Files, s: Natural, user: User] {
		//pre conditions
		user in GitBob.register_user.t // 16. The owner of a GitBob file should be a registered user;
		file not in GitBob.files.t //15. One cannot add to GitBob already existing files;
		//Actions
		file.size = s
		file.owner = user
		file.version.t' = increment[na/Zero]//17. The initial version of a file is number 1; 
		GitBob.files.t' = GitBob.files.t + file
		file.shared.t' = user //22. A GitBob file is always shared with its owner;
		file.mode.t' = Regular //32. Files are by default shared in REGULAR mode;
		user.local_files.t' = user.local_files.t + file
		//post conditions
		noUserChangeExcept[t, t', user]
		noGitBobUsersEmailsChangeExcept[t, t']
		noFileVersionChangeExcept[t,t', GitBob.files.t]
		noFileSharesChangeExcept[t, t', GitBob.files.t]
		noUserLocalFilesChangeExcept[t, t', GitBob.register_user.t]
}

pred removeFile  [t, t' : Time , f: Files, user: User] {
		//pre condition
 		user in GitBob.register_user.t
		f in GitBob.files.t //18. Only existing files may be removed from GitBob;
		user in f.shared.t //25. Files can only be removed by users that have access to that file;
		f.mode.t = ReadOnly => user = f.owner //33. READONLY files can only be removed from GitBob by its owner;
		//Actions
		GitBob.files.t' = GitBob.files.t - f
		//post conditions
		noUserChangeExcept[t, t', user]
		noGitBobUsersEmailsChangeExcept[t, t']
		noFileVersionChangeExcept[t,t', GitBob.files.t]
		noFileSharesChangeExcept[t, t', GitBob.files.t]
}


pred uploadFile  [t, t' : Time , f: Files, user: User] {
		//Pre conditions
		user in GitBob.register_user.t 
		f in user.local_files.t
		f in GitBob.files.t // 18. Only existing files may be uploaded from GitBob;
		user in f.shared.t //25. Files can only be upload by users that have access to that file;
		f.mode.t = ReadOnly => user = f.owner //34. New versions of READONLY files can only be uploaded by its owner;
		GitBob.files.t.version.t = user.local_files.t.version.t
		//Actions
		GitBob.files.t'.version.t' = increment[GitBob.files.t.version.t] //19. Upon a successful upload, the version of a file is increased by one;
		user.local_files.t'.version.t' = increment[user.local_files.t.version.t]
		//post conditions
		noUserChangeExcept[t, t', user]
		noFileModeChangeExcept[t, t', f]
		noFileSharesChangeExcept[t, t', f]
		noGitBobUsersEmailsChangeExcept[t, t']
}

pred downloadFile  [t, t' : Time , f: Files, user: User] {
		//Pre conditions
		user in GitBob.register_user.t // 18. Only existing files may be downloaded from GitBob;
		user in f.shared.t //25. Files can only be download by users that have access to that file;
		f in GitBob.files.t
		//Actions
		user.local_files.t' = user.local_files.t ++ f
		//post conditions
		noFileModeChangeExcept[t, t', f]
		noFileSharesChangeExcept[t, t', f]
		noFileVersionChangeExcept[t,t', GitBob.files.t]
		noGitBobFilesChangeExcept[t, t']
		noGitBobUsersEmailsChangeExcept[t, t']
}

pred shareFile [t, t' : Time , f: Files, user1, user2: User]{
		//Pre conditions
		user1 in GitBob.register_user.t
		user2 in GitBob.register_user.t  // 21. A GitBob file can only be shared among registered users;
		user1 in f.shared.t // 26. A user with access to a file may share it with some other user;
		user2 not in f.shared.t //27. One can only share a file with users that do not share yet that same file;
		f.mode.t = Secure => user2.type.t = Premium //30. A file can only be shared securely, if all the users that have access to it are PREMIUM;
		f in GitBob.files.t
		//Actions
		f.shared.t' = f.shared.t + user2
		downloadFile[t, t', f, user2]
		//post conditions
		noUserChangeExcept[t, t', user1]
		noUserChangeExcept[t, t', user2]
		noFileModeChangeExcept[t, t', f]
		noGitBobFilesChangeExcept[t, t']
		noGitBobUsersEmailsChangeExcept[t, t']
}

pred removeShare [t, t' : Time , f: Files, user1, user2: User]{
		//Pre conditions
		user1 in GitBob.register_user.t
		user2 in GitBob.register_user.t
		user1 in f.shared.t
		user2 in f.shared.t
		user2 != f.owner //28. A user with access to a file may stop the sharing of that file with any other user, except with its owner;
		f in GitBob.files.t
		//Actions
		f.shared.t' = f.shared.t - user2
		user2.local_files.t' = user2.local_files.t - f
		//post conditions
		noGitBobFilesChangeExcept[t, t']
		noUserChangeExcept[t, t', user1]
		noUserChangeExcept[t, t', user2]
		noUserLocalFilesChangeExcept[t, t', user1]
		noFileModeChangeExcept[t, t', f]
		noFileVersionChangeExcept[t,t', GitBob.files.t]
		noGitBobUsersEmailsChangeExcept[t, t']
}

pred changeSharingMode[t, t' : Time , f: Files, user: User, m: Mode]{
		//Pre conditions
		user in GitBob.register_user.t
		f in GitBob.files.t
		user = f.owner //35. Only the user that is the owner of a file may change its sharing mode;
		m = Secure => all ty : f.shared.t.type.t | ty = Premium //36. One can only change the sharing mode of a file to SECURE if all users that have access to the file are PREMIUM;
		//Actions
		f.mode.t' = m
		//post conditions
		noFileVersionChangeExcept[t,t', GitBob.files.t]
		noGitBobUsersEmailsChangeExcept[t, t']
		noGitBobFilesChangeExcept[t, t']
		noUserChangeExcept[t, t', user]
		noFileSharesChangeExcept[t, t', f]
		noUserLocalFilesChangeExcept[t, t', GitBob.register_user.t]
}




// Post Conditions

pred noGitBobUsersEmailsChangeExcept[t, t' : Time] {
	GitBob.register_user.t = GitBob.register_user.t'
}

pred noUserChangeExcept[t, t' : Time, user: User] {
	user.type.t = user.type.t'
}

pred noUserLocalFilesChangeExcept[t, t' : Time, user: User] {
	user.local_files.t = user.local_files.t'
}

pred noFileModeChangeExcept[t, t' : Time, file: Files] {
	file.mode.t = file.mode.t'
}

pred noFileSharesChangeExcept[t, t' : Time, file: Files] {
	file.shared.t = file.shared.t'
}

pred noFileVersionChangeExcept[t, t' : Time, file: Files] {
	file.version.t = file.version.t'
}

pred noGitBobFilesChangeExcept[t, t' : Time] {
	GitBob.files.t = GitBob.files.t'
}



//Functions
fun increment [n: Natural] : lone Natural { 
	na/next[n] 
}



fact traces {
  init [to/first]
  all t: Time - to/last | let t' = t.next |
	some  user, user2: User, type : UTypes, email : UEmail, file :Files, size: Natural, m: Mode|
      newUser[t, t', user, type, email] || removeUser[t, t', user] ||upgradePremium[t, t', user] || downgradeBasic[t, t', user] ||
 	  addFile[t, t', file, size, user] || removeFile[t, t', file, user] || uploadFile[t, t', file, user] || downloadFile[t, t', file, user] ||
	  shareFile [t, t' , file , user, user2] || removeShare [t, t' , file , user, user2] || changeSharingMode[t, t', file, user, m]
}



// ASSERTS

//3. The e-mails registered in GitBob are unique;
assert EmailUnique {
	all t:Time, user1, user2: GitBob.register_user.t |
		user1 != user2 => user1.email != user2.email	
}

//2. All users registered in GitBob have a profile type, and an e-mail;
assert UserHasEmailType{
	all t: Time, user : GitBob.register_user.t | some mail: UEmail | some ty: UTypes| 
		user.email = mail && user.type.t = ty                     				 
}

//10. Every file in GitBob occupies a given space, has a (single) owner, and is in a given version;
assert FileHasVersionSizeMode{
	all t: Time, file : GitBob.files.t | some si: Natural, v: Natural, own : User, m: Mode |   
		file.size = si && file.version.t = v && file.owner = own && file.mode.t = m          
}

assert FileVersionBiggerThan1{
	all t: Time, file: GitBob.files.t |
		na/gt[file.version.t, na/Zero]
}

// 13. At the beginning there are no files in GitBob;
assert BeginWithoutData{
	no GitBob.files.to/first
}

// 21. A GitBob file can only be shared among registered users;
assert SharedAmongRegisteredUsers{
	all t: Time, files: GitBob.files.t, user: files.shared.t |
			user in GitBob.register_user.t
}

//22. A GitBob file is always shared with its owner;
assert AlwaysSharedWithOwner{ 
	all t: Time, file : GitBob.files.t | file.owner in file.shared.t
}

//30. A file can only be shared securely, if all the users that have access to it are PREMIUM;
assert SecureShareAllPremium{ 
	all t: Time, file : GitBob.files.t, user :  file.shared.t | file.mode.t = Secure => user.type.t = Premium
}


pred simulate() {}
run simulate for 1 GitBob, 10 User,  10 UEmail, 10 Files, 10 Natural,  10 Time
check EmailUnique for 1 GitBob, 10 User,  10 UEmail, 10 Files, 10 Natural,  10 Time
check UserHasEmailType for 1 GitBob, 10 User,  10 UEmail, 10 Files, 10 Natural,  10 Time
check FileHasVersionSizeMode  for 1 GitBob, 10 User,  10 UEmail, 10 Files, 10 Natural,  10 Time
check FileVersionBiggerThan1 for 1 GitBob, 10 User,  10 UEmail, 10 Files, 10 Natural,  10 Time
check BeginWithoutData for 1 GitBob, 10 User,  10 UEmail, 10 Files, 10 Natural,  10 Time
check SharedAmongRegisteredUsers  for 1 GitBob, 10 User,  10 UEmail, 10 Files, 10 Natural,  10 Time
check AlwaysSharedWithOwner for 1 GitBob, 10 User,  10 UEmail, 10 Files, 10 Natural,  10 Time
check SecureShareAllPremium for 1 GitBob, 10 User,  10 UEmail, 10 Files, 10 Natural,  10 Time
